; Name   : main.asm
; Created: 11/11/2017 11:00:00 AM
; Author : Keith Webb
; Purpose: Main application code for testing the ATTiny104 on the Xplained Dev Kit

; Start data segment 
.DSEG

; RAM Variables
led_loop_state: .BYTE 1		; Stores whether led_logic is active

; Start code segment
.CSEG

start:
	; code entry point, perform initialization tasks here
	cli								; disable Interrupts, which can cause setup functions to fail
	rcall setup_clock				; setup Clock to be 8Mhz
	rcall setup_sram				; fill SRAM with zeros
	rcall setup_gpio				; setup LED and Button GPIO		
	rcall setup_usart				; setup USART to allow IO
	sei								; enable Interrupts
    rjmp loop						; start main application loop

setup_clock:
	; sets up clock to run at full speed
	; Note: once 0xD8 is written to CCP, you have 4 cycles to modify CCP protected registers
	ldi r16, 0xD8					; write signature for enabling write to protected IO
	ldi r17, CLKPS0					; write desired clock prescalar (CLKPS0=0, Div by 1)
	out CCP, r16					; allow protected register writes
	out CLKPSR, r17					; write clock prescalar to CLKPSR
	ret								; clock setup, exit function

setup_usart:
	; sets up USART module for communications

	; set baud rate to UBRR (0x0001 = 250,000bps if Clock=8MHz)
	ldi r17, 0x00					; hardcode UBRRH to 0
	ldi r16, 0x01					; hardcode UBRRL to 1
	out UBRRH, r17					; store setting
	out UBRRL, r16					; store setting
	
	ldi r16, (1<<RXEN)|(1<<TXEN)	; enable receiver and transmitter
	out UCSRB, r16					; store settings
	ldi r16, (1<<USBS)|(3<<UCSZ0)	; set frame format: 8 data bits, 2 stop bits
	out UCSRC, r16					; store settings

	ret								; setup done, exit function

setup_gpio:
	; sets up GPIO to desired settings
	sbi DDRA, 5						; Sets PA5 as output	
	sbi PUEB, 1						; Enable pull up on PB1	
	cbi DDRB, 1						; Sets PB1 as input	
	sbi PORTA, 5					; Turn LED Off
	ret

setup_sram:
	; initializes SRAM to be filled with zeros
	ldi ZL, low(SRAM_START)			; load ZL pointer with SRAM start location
	ldi ZH, low(SRAM_START)			; load ZH pointer with SRAM start location
	ldi r16, 0						; load r16 with 0, using r16 as iterator
	ldi r17, 0						; load r17 with value to set SRAM bytes to	

	setup_sram_loop_start:			; start of loop, will be called SRAM_SIZE times
	st Z+, r17						; store r17 to Z location and increment Z pointer
	inc r16							; increment counter
	cpi r16, SRAM_SIZE				; compare counter with SRAM Size, which must be <= 255!
	brne setup_sram_loop_start		; if iterator not equal to SRAM Size, loop to next location

	ret								; SRAM is filled, exit function

write_usart:
	; writes data to USART module, value goes in r16

	; Usage:
	; ldi r16, 'H'		; load desired byte into r16
	; rcall write_usart ; transmit byte with USART
	
	; wait for empty transmit buffer
	in r17, UCSRA					; get USART Control and Status Register A
	sbrs r17, UDRE					; skip jump if tx buffer is empty
	rjmp write_usart				; jump back if tx buffer is not empty
	
	out UDR, r16					; put data (r16) into buffer, sends the data

	ret								; data written, exit function

read_usart:
	; reads data from USART module, result in r16

	; Example
	; rcall read_usart	; get data from USART
	; ...				; do something with data in r16

	; wait for data to be received
	in r17, UCSRA					; get USART Control and Status Register A
	sbrs r17, RXC					; skip jump if data in rx buffer
	rjmp read_usart					; jump back if rx buffer is empty
	
	in r16, UDR						; get and return received data from buffer

	ret								; data read, exit function

write_str_usart:
	; writes a null terminated string to the USART
	
	; Usage:
	; ldi ZL, low(some_location)								; load string location
	; ldi ZH, high(some_location) <+ 0x40 if in program space>	; load string location
	; rcall write_str_usart										; call function	

	ld r16, Z+						; load value pointed to by Z and then increment Z
 
	cpi r16, 0						; check if r16 has null character
	breq write_str_usart_done		; if r16 has null, jump to function exit

	rcall write_usart				; r16 has desired byte, call write function
	rjmp write_str_usart			; repeat this whole function

	write_str_usart_done:			; label for function exit
	ret								; string has been written, exit function

flush_rx_usart:
	; flushes USART Rx Buffer
	in r16, UCSRA					; get USART Control and Status Register A
	sbrs r16, RXC					; skip ret if rx buffer has data
	ret								; return if rx buffer is empty
	in r16, UDR						; pull out data from rx buffer
	rjmp flush_rx_usart				; repeat to clear all data from buffer

data_waiting_usart:
	; loads true/false into r16 if USART rx has data
	ldi r16, 0						; assume no data, load false
	in r17, UCSRA					; get USART Control and Status Register A
	sbrs r17, RXC					; skip return false if rx buffer has data
	ret								; return false, no data
	ldi r16, 1						; have data, load true
	ret								; return true, have data

loop:
	; perform main loop tasks here
	rcall data_waiting_usart		; check if data in buffer
	sbrc r16, 0						; skip next line if bit 0 is cleared, means no data to process
	rcall handle_data				; if we have data, call handler
	
	ldi r16, 0						; load false case into r16
	lds r17, led_loop_state			; pull variable from RAM
	cpse r16, r17					; skip led_logic if variable is 0
	rcall led_logic					; if variable is 1, handle LED logic

	rjmp loop						; repeat loop endlessly

handle_data:
	; takes 1 of 5 actions depending on data from USART
	rcall read_usart				; get USART byte
	
	cpi r16, 'H'					; is USART byte an 'H'?
	breq handle_data_h				; byte is an 'H', run handler
	
	cpi r16, 'B'					; is USART byte an 'B'?
	breq handle_data_b				; byte is a 'B', run handler
	
	cpi r16, 'O'					; is USART byte an 'O'?
	breq handle_data_o				; byte is an 'O', run handler
	
	cpi r16, 'P'					; is USART byte an 'P'?
	breq handle_data_p				; byte is a 'P', run handler
	
	ret								; else case, exit function

	handle_data_h:					; handle byte is 'H' case
	cbi PORTA, 5					; turn LED On
	ldi ZL, low(hi_str*2)			; load address of message string
	ldi ZH, high(hi_str*2) + 0x40	; Data Mapped Program Memory Starts at: 0x4000, remember offset!
	rcall write_str_usart			; write message string
	ret								; data handled, exit function 
	
	handle_data_b:					; handle byte is 'B' case
	sbi PORTA, 5					; turn LED Off
	ldi ZL, low(bye_str*2)			; loads address of message string
	ldi ZH, high(bye_str*2) + 0x40	; Data Mapped Program Memory Starts at: 0x4000, remember offset!
	rcall write_str_usart			; write message string
	ret								; data handled, exit function 

	handle_data_o:					; handle byte is 'O' case
	ldi r16, 1						; load r16 with future variable value, 1 means on
	sts led_loop_state, r16			; set variable in RAM
	ret								; data handled, exit function 

	handle_data_p:					; handle byte is 'P' case
	ldi r16, 0						; load r16 with future variable value, 0 means off
	sts led_loop_state, r16			; set variable in RAM
	sbi PORTA, 5					; turn LED Off
	ret								; data handled, exit function 

led_logic:
	; sets LED state to be the inverse of the button state
	; Note: PB1: 1 = not pressed, 0 = pressed
	; Note: PA5: 1 = LED Off,     1 = LED On
	in r17, PORTA					; Load Output State from PortA
	in r16, PINB					; Load Inputs from PortB
	com r16							; Invert Inputs from PortB
	bst r16, 1						; Copy Button Bit to Storage
	bld r17, 5						; Restore Button Bit to LED Bit
	out PORTA, r17					; Set LED Based on Button Bit
	ret								; logic handled, exit function

forever:
	; catches certain run on bugs, should not execute
	rjmp forever					; loop endlessly

; Code Space Data Values, strings must be NULL terminated for use in 'write_str_usart' function
hi_str: .db "Hi", '\n', 0			; string printed by USART when 'H' is recieved
bye_str: .db "Bye", '\n', 0, 0		; string printed by USART when 'B' is recieved, extra 0 is for flash padding

; catches certain run on bugs, should not execute
rjmp forever						; loop endlessly
